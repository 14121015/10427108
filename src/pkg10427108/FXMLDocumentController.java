/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10427108;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;
    Connection conn;
    @FXML
    private Button button;
    @FXML
    private Button button1;
    @FXML
    private Button button2;

    @FXML
    private void handleButtonAction(ActionEvent event) {
        doconn();
    }

    @FXML
    private void handleButtonAction2(ActionEvent event) {
        label.setText(Integer.toString(doInsert()));
    }

    @FXML
    private void handleButtonAction3(ActionEvent event) {
        label.setText(Integer.toString(doselect()));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    private int doInsert() {
        int rows = 0;
        if (conn == null) {
            doconn();
        }
        try {
            String select = "select * from TEST.COLLEAGUES";
            PreparedStatement stmts = conn.prepareStatement(select);
            int maxid=0;
            ResultSet result = stmts.executeQuery();
            while (result.next()) {
                maxid = result.getInt("ID");
            }
            
            String insert = "insert into TEST.COLLEAGUES values (?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(insert);
            stmt.setInt(1, maxid+1);
            stmt.setString(2, "test");
            stmt.setString(3, "test");
            stmt.setString(4, "test");
            stmt.setString(5, "test");
            stmt.setString(6, "test");
            stmt.setInt(7, 0);
            rows = stmt.executeUpdate();

            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return rows;
        }
    }

    private void doconn() {
        try {
            conn = DriverManager.getConnection("jdbc:derby://localhost:1527/contact", "test", "test");
            if (conn != null) {
                label.setText("ConnectOK");

            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label.setText("ConnectFail");
        }
    }

    private int doselect() {
        int rows = 0;
        if (conn == null) {
            doconn();
        }
        try {
            String select = "select * from TEST.COLLEAGUES";
            PreparedStatement stmt = conn.prepareStatement(select);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                System.out.println(result.getString("FIRSTNAME"));
                rows++;
            }
            result.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return rows;
        }
    }

}
